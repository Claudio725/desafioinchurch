//
//  DetalheViewController.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 04/06/21.
//

import UIKit
import Alamofire
import AlamofireImage

class DetalheViewController: UIViewController {
    
    var titulo: String!
    var poster: String!
    var releaseDate : String!
    var tipoFilme: String!
    var descricao: String!
    var linha: Int!
    var detalhe_arr:[Result]!=[]
    
    @IBOutlet weak var imagem : UIButton!
    @IBOutlet var releasedate: UILabel!
    @IBOutlet var tipo: UILabel!
    @IBOutlet var descric : UILabel!
    
    var favorito: [Result]!
    
    var Generos: Generos!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Carregar no endpoint https://bitbucket.org/Claudio725/inchurch-ios-challenge-claudio/src/master/
        
        Generos = Bundle.main.decode_Generos()
        
        // Do any additional setup after loading the view.
        title = titulo
        //Carrega o poster do filme
        let StrUrl: URL = URL(string: "http://image.tmdb.org/t/p/w500" + poster)!

        imagem.af.setImage(for: .normal, url: StrUrl)
        
        //Carrega o genero do filme, releasedate e descricao
        tipo.text = Generos.genres[linha].name
        
        //Formatar data
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"

        let date: Date? = dateFormatterGet.date(from: releaseDate)
        releasedate.text = dateFormatter.string(from: date!)

        
        descric.text = descricao
        
        //Colocar a figura Star para permitir favoritar
        //na barra superior
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "star.png"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
        btn1.addTarget(self, action: #selector(addCard(sender:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        let buttons : NSArray = [item1]
        self.navigationItem.rightBarButtonItems = buttons as? [UIBarButtonItem]
    }
    
    //Favoritar
    @objc func addCard(sender: AnyObject) {
        lerFavoritos()
        
        do {
            favorito.append(contentsOf: detalhe_arr)
            let pData = try! JSONEncoder().encode(favorito)

            UserDefaults.standard.set(pData, forKey: "Favorito")
            
            //Mostra alerta
            mensagem(message: "Favourited")
        }
    }
    
    //Processo de Ler favoritos para poder appendar novos registros
    func lerFavoritos() {
        let chaveLida = UserDefaults.standard.data(forKey: "Favorito")
        favorito = try! JSONDecoder().decode([Result].self, from: chaveLida!)
    }

    //Mensagem customizada
    func mensagem( message: String) {
        let alertController = UIAlertController(title: "The Movies",
                                    message: message,
                                    preferredStyle: .alert)
        
        present(alertController, animated: true, completion: nil)
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertController.dismiss(animated: true, completion: nil)
        }
    }

}





