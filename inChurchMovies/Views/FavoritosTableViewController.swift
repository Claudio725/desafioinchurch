//
//  FavoritosTableViewController.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 06/06/21.
//

import UIKit
import Alamofire
import AlamofireImage

private let reuseIdentifier = "cell"

class FavoritosTableViewController: UITableViewController,UISearchResultsUpdating {

        var FilmesFiltrados: [Result] = []
        var favorito:[Result] = []

        @IBOutlet var grid: UITableView!
        
        var currentCellNumber: Int!
        
        var resultSearchController = UISearchController(searchResultsController: nil)
        
        let defaults = UserDefaults.standard
        
        override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
            //1 - Favoritos - todas
            lerFavoritos()
 
            //2 - results recebe a array Result
            FilmesFiltrados = favorito
            
            //3 - setar o datasource e o delegate
            self.grid.delegate = self
            
            self.grid.delegate = self
            self.grid.dataSource = self
            
            self.grid.reloadData()

            
            title = "Favourites"
            
            //search controller
            resultSearchController = ({
                // 1
                let controller = UISearchController(searchResultsController: nil)
                controller.searchResultsUpdater = self
                // 2
                controller.obscuresBackgroundDuringPresentation = false
                // 3
                controller.searchBar.placeholder = "Search Films"
                controller.searchBar.sizeToFit()
                // 4
                navigationItem.searchController = controller
                // 5
                definesPresentationContext = true
                return controller
            })()
            
            //Favoritos - processo de leitura
            lerFavoritos()

        }
        

    //Processo de Ler favoritos
    func lerFavoritos() {
        let chaveLida = UserDefaults.standard.data(forKey: "Favorito")
        if chaveLida != nil {
            favorito = try! JSONDecoder().decode([Result].self, from: chaveLida!)
        } else {
            favorito = []
        }
    }
        
    //Métodos do searchcontroller
    func updateSearchResults(for searchController: UISearchController) {
        FilmesFiltrados.removeAll(keepingCapacity: false)

        let searchPredicate = resultSearchController.searchBar.text!
        
        let array = favorito.filter{ favoritoL in
            return favoritoL.title.contains(searchPredicate)
        }
        
        FilmesFiltrados = array

        self.grid.reloadData()
    }


    // MARK: UICollectionViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //filtro searchbar
        if (resultSearchController.isActive) {
          return FilmesFiltrados.count
        } else {
            return favorito.count
        }
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        // Configure the cell
        let cellIdentifier = "cell"
        
        let celula = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        as! MyCellFavorito
        
        celula.imagem?.tag = indexPath.row
        celula.releaseDate?.tag = indexPath.row
        celula.tituloF?.tag = indexPath.row
        celula.descricao?.tag = indexPath.row
        
        //Verificar o filtro
        if resultSearchController.isActive {
            //Filtra os resultados
            //Carrega o poster do filme
            let StrUrl: URL = URL(string: "http://image.tmdb.org/t/p/w500" + FilmesFiltrados[indexPath.row].posterPath)!

            celula.imagem.af.setImage(for: .normal, url: StrUrl)

            //Carrega o titulo do filme
            celula.tituloF?.text = FilmesFiltrados[indexPath.row].title
            //Carrega a releaseDate
            //Formatar data
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"

            let date: Date? = dateFormatterGet.date(from: FilmesFiltrados[indexPath.row].releaseDate)
            celula.releaseDate?.text = dateFormatter.string(from: date!)

            //Carrega a descricao
            celula.descricao?.text = FilmesFiltrados[indexPath.row].overview

        } else {
        
            //Carrega o poster do filme
            let caminhoPoster = "http://image.tmdb.org/t/p/w500" + favorito[indexPath.row].posterPath
            let StrUrl: URL = URL(string: caminhoPoster)!

            celula.imagem.af.setImage(for: .normal, url: StrUrl)

            //Carrega o titulo do filme
            celula.tituloF?.text = favorito[indexPath.row].title
            //Carrega a releaseDate
            //Formatar data
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"

            let date: Date? = dateFormatterGet.date(from: favorito[indexPath.row].releaseDate)
            celula.releaseDate?.text = dateFormatter.string(from: date!)
            //Carrega a descricao
            celula.descricao?.text = favorito[indexPath.row].overview
        }
        
        return celula

    }
}
