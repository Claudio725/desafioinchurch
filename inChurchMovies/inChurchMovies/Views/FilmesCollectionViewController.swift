//
//  FilmesCollectionViewController.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 04/06/21.
//

import UIKit
import Alamofire
import AlamofireImage

private let reuseIdentifier = "cell"

class FilmesCollectionViewController: UICollectionViewController, UISearchResultsUpdating {

    var movies: Movies? = nil
    var results: [Result] = []
    var FilmesFiltrados: [Result] = []
    var favorito:[Result]! = []

    @IBOutlet weak var gridFilmes: UICollectionView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var currentCellNumber: Int!
    
    var resultSearchController = UISearchController(searchResultsController: nil)
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !Connectivity.isConnectedToInternet {
            title = "The Movies"
            self.collectionView.setEmptyMessage("Sem conexão com a internet")
        } else {
        
            // Do any additional setup after loading the view.
            //1 - Especialidades - todas
            movies = Bundle.main.decode()
            createSpinnerView()
            
            //2 - results recebe a array Result
            results = movies?.results ?? []
            FilmesFiltrados = movies?.results ?? []
            
            //Favoritos - processo de leitura
            lerFavoritos()
            
            //3 - setar o datasource e o delegate
            self.gridFilmes.delegate = self
            self.gridFilmes.dataSource = self
            
            self.gridFilmes.reloadData()

            
            title = "The Movies"
            
            //search controller
            resultSearchController = ({
                // 1
                let controller = UISearchController(searchResultsController: nil)
                controller.searchResultsUpdater = self
                // 2
                controller.obscuresBackgroundDuringPresentation = false
                // 3
                controller.searchBar.placeholder = "Search Films"
                controller.searchBar.sizeToFit()
                // 4
                navigationItem.searchController = controller
                // 5
                definesPresentationContext = true
                return controller
            })()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lerFavoritos()
        gridFilmes.reloadData()
    }
    
    func createSpinnerView() {
        let child = SpinnerViewController()

        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)

        // wait two seconds to simulate some work happening
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // then remove the spinner view controller
            child.willMove(toParent: nil)
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
    }
    
    //Processo de Gravar favoritos
    func salvarFavoritos(favorito_arr: [Result]) {
        do {
            let pData = try! JSONEncoder().encode(favorito_arr)
//            let Fdata = try? NSKeyedArchiver.archivedData(withRootObject: favorito_arr as Array, requiringSecureCoding: false)
            UserDefaults.standard.set(pData, forKey: "Favorito")
            
            //Mostra alerta
            let alert = UIAlertController()
            alert.title = "THe Movies"
            alert.message = "Filme adicionado para Favoritos"
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    
    //Processo de Ler favoritos
    func lerFavoritos() {
        let chaveLida = UserDefaults.standard.data(forKey: "Favorito")
        if chaveLida != nil {
            favorito = try! JSONDecoder().decode([Result].self, from: chaveLida!)
            print (favorito.count)
        } else {
            favorito = []
        }
    }
    
    
    //Métodos do searchcontroller
    func updateSearchResults(for searchController: UISearchController) {
        FilmesFiltrados.removeAll(keepingCapacity: false)

        let searchPredicate = resultSearchController.searchBar.text!
        
        let array = results.filter{ result in
            return result.title.contains(searchPredicate)
        }
        
        FilmesFiltrados = array

        self.gridFilmes.reloadData()
    }


    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        
        //filtro searchbar
        if (resultSearchController.isActive) {
            if !Connectivity.isConnectedToInternet {
                self.collectionView.setEmptyMessage("Sem conexão com a internet")
                return favorito.count
            }
            return FilmesFiltrados.count
        } else {
            if !Connectivity.isConnectedToInternet {
                self.collectionView.setEmptyMessage("Sem conexão com a internet")
                return favorito.count
            }
            return results.count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        // Configure the cell
        let cellIdentifier = "cell"
    
        let celula = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MyCell
        
        celula.imagem?.tag = indexPath.row
        celula.favorito?.tag = indexPath.row
        celula.titulo?.tag = indexPath.row
                
        //Verificar o filtro
        if resultSearchController.isActive {
            //Filtra os resultados
            //Carrega o poster do filme
            let StrUrl: URL = URL(string: "http://image.tmdb.org/t/p/w500" + FilmesFiltrados[indexPath.row].posterPath)!

            celula.imagem.af.setImage(for: .normal, url: StrUrl)

            //Carrega o titulo do filme
            celula.titulo?.text = FilmesFiltrados[indexPath.row].title
            
            //Carrega a estrela (nao favorito como default - ajustar para favoritar
            
            //Verifica se o valor lido do array Favorito for diferente de null
            if (favorito.count > 0) {
                for i in 0...favorito.count-1 {
                    if (favorito[i].title == FilmesFiltrados[indexPath.row].title) {
                        celula.favorito?.setImage(UIImage(named: "star-fill.png"), for: .normal)
                        break;
                    } else {
                        celula.favorito?.setImage(UIImage(named: "star.png"), for: .normal)
                    }
                }
            }
        } else {
        
            //Carrega o poster de todos os filmes
            let StrUrl: URL = URL(string: "http://image.tmdb.org/t/p/w500" + results[indexPath.row].posterPath)!

            celula.imagem.af.setImage(for: .normal, url: StrUrl)

            //Carrega o titulo do filme
            celula.titulo?.text = results[indexPath.row].title
            
            //Carrega a estrela (nao favorito como default - ajustar para favoritar
            
            //Verifica se o valor lido do array Favorito for diferente de null
            if (favorito.count > 0) {
                for i in 0...favorito.count-1 {
                    if (favorito[i].title == results[indexPath.row].title) {
                        celula.favorito?.setImage(UIImage(named: "star-fill.png"), for: .normal)
                        break;
                    } else {
                        celula.favorito?.setImage(UIImage(named: "star.png"), for: .normal)
                    }
                }
            }
            
        }
        
        return celula

    }
    
    @IBAction func favoritar(sender: UIButton) {
        currentCellNumber = (sender.tag)
        favorito.append(results[currentCellNumber])
        salvarFavoritos(favorito_arr: favorito)
    }
    
    //Trata clique na imagem para mostrar o detalhe
    @IBAction func detalhar(sender: UIButton) {
        currentCellNumber = (sender.tag)
        performSegue(withIdentifier: "detalhar", sender: sender)
    }
    

    //Passar os dados pesquisados para a página Detalhada com o filme selecionado
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            if identifier == "detalhar" {
                if resultSearchController.isActive == false{
                    if (gridFilmes?.indexPathsForSelectedItems) != nil {
                        let detalheViewController = segue.destination as! DetalheViewController
                        //Passa os detalhes do filme
                        detalheViewController.titulo = results[currentCellNumber].title
                        detalheViewController.poster = results[currentCellNumber].posterPath
                        detalheViewController.releaseDate =  results[currentCellNumber].releaseDate
                        detalheViewController.tipoFilme =  results[currentCellNumber].originalTitle
                        detalheViewController.descricao = results[currentCellNumber].overview
                        detalheViewController.linha = currentCellNumber
                        detalheViewController.detalhe_arr.append( results[currentCellNumber])
                    }
                } else {
                    if (gridFilmes?.indexPathsForSelectedItems) != nil {
                        let detalheViewController = segue.destination as! DetalheViewController
                        //Passa os detalhes do filme
                        detalheViewController.titulo = FilmesFiltrados[currentCellNumber].title
                        detalheViewController.poster = FilmesFiltrados[currentCellNumber].posterPath
                        detalheViewController.releaseDate =  FilmesFiltrados[currentCellNumber].releaseDate
                        detalheViewController.tipoFilme =  FilmesFiltrados[currentCellNumber].originalTitle
                        detalheViewController.descricao = FilmesFiltrados[currentCellNumber].overview
                        detalheViewController.linha = currentCellNumber
                        detalheViewController.detalhe_arr.append( FilmesFiltrados[currentCellNumber])
                    }
                }
            }
        }
    }

}

extension UICollectionView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        //let messageLabel = UILabel(frame: CGRect(x: 50,y: 50,width: 300,height: 21))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
        messageLabel.sizeToFit()
        
        messageLabel.text = message
        messageLabel.textAlignment = .center
        
        let image: UIImage = UIImage(named: "smiley")!

        var bgImage: UIImageView?
        bgImage = UIImageView(image: image)
        bgImage!.frame = CGRect(x: 155,y: 220,width: 60,height: 60)
        
        messageLabel.addSubview(bgImage!)
        
        self.backgroundView = messageLabel
        //self.separatorStyle = .none
    }
    

    func restore() {
        self.backgroundView = nil
        //self.separatorStyle = .singleLine
    }
}

