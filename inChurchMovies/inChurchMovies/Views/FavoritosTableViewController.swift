//
//  FavoritosTableViewController.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 06/06/21.
//

import UIKit
import Alamofire
import AlamofireImage

private let reuseIdentifier = "cell"

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}

class FavoritosTableViewController: UITableViewController,UISearchResultsUpdating {

    var FilmesFiltrados: [Result] = []
    var favorito:[Result] = []

    @IBOutlet var grid: UITableView!
    
    var currentCellNumber: Int!


    var resultSearchController = UISearchController(searchResultsController: nil)
    
    let defaults = UserDefaults.standard
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !Connectivity.isConnectedToInternet {
            title = "Favorites"
            self.tableView.setEmptyMessage("Sem conexão com a internet")
        } else {

            createSpinnerView()
            
            // Do any additional setup after loading the view.
            //1 - Favoritos - todas
            lerFavoritos()

            //2 - results recebe a array Result
            FilmesFiltrados = favorito
            
            //3 - setar o datasource e o delegate
            self.grid.delegate = self
            
            self.grid.delegate = self
            self.grid.dataSource = self
            
            self.grid.reloadData()

            
            title = "Favorites"
            
            //search controller
            resultSearchController = ({
                // 1
                let controller = UISearchController(searchResultsController: nil)
                controller.searchResultsUpdater = self
                // 2
                controller.obscuresBackgroundDuringPresentation = false
                // 3
                controller.searchBar.placeholder = "Search Films"
                controller.searchBar.sizeToFit()
                // 4
                navigationItem.searchController = controller
                // 5
                definesPresentationContext = true
                return controller
            })()
            
            
            self.grid.allowsSelectionDuringEditing = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lerFavoritos()
        grid.reloadData()
    }
      
    func createSpinnerView() {
        let child = SpinnerViewController()

        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)

        // wait two seconds to simulate some work happening
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // then remove the spinner view controller
            child.willMove(toParent: nil)
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
    }

    //Processo de atualizar favoritos
    func salvarFavoritos(favorito_arr: [Result]) {
        if favorito_arr.count >= 0 {
            do {
                let pData = try! JSONEncoder().encode(favorito_arr)

                UserDefaults.standard.set(pData, forKey: "Favorito")
                
            }
        }
    }
    
    //Processo de Ler favoritos
    func lerFavoritos() {
        let chaveLida = UserDefaults.standard.data(forKey: "Favorito")
        if chaveLida != nil {
            favorito = try! JSONDecoder().decode([Result].self, from: chaveLida!)
        } else {
            favorito = []
        }
        print (favorito.count)
    }
        
    //Métodos do searchcontroller
    func updateSearchResults(for searchController: UISearchController) {
        FilmesFiltrados.removeAll(keepingCapacity: false)

        let searchPredicate = resultSearchController.searchBar.text!
        
        let array = favorito.filter{ favoritoL in
            return favoritoL.title.contains(searchPredicate)
        }
        
        FilmesFiltrados = array

        self.grid.reloadData()
    }
    
    // this method handles row deletion
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {

            // remove the item from the data model
            favorito.remove(at: indexPath.row)
            salvarFavoritos(favorito_arr: favorito)

            // delete the table view row
            self.grid.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Delete"
    }


    // MARK: UICollectionViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        //filtro searchbar
        if (resultSearchController.isActive) {
            //testa se internet
            if !Connectivity.isConnectedToInternet {
                self.tableView.setEmptyMessage("Sem conexão com a internet")
                return FilmesFiltrados.count
            }
            
            if FilmesFiltrados.count == 0 {
                self.tableView.setEmptyMessage("You do not have any favorites yet.")
            } else {
                self.tableView.restore()
            }
            return FilmesFiltrados.count
        } else {
            //testa se internet
            if !Connectivity.isConnectedToInternet {
                self.tableView.setEmptyMessage("Sem conexão com a internet")
                return favorito.count
            }
            
            if favorito.count == 0 {
                self.tableView.setEmptyMessage("You do not have any favorites yet.")
            } else {
                self.tableView.restore()
            }
            return favorito.count
        }
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        // Configure the cell
        let cellIdentifier = "cell"
        
        let celula = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        as! MyCellFavorito
        
        celula.imagem?.tag = indexPath.row
        celula.releaseDate?.tag = indexPath.row
        celula.tituloF?.tag = indexPath.row
        celula.descricao?.tag = indexPath.row
        
        //Verificar o filtro
        if resultSearchController.isActive {
            //Filtra os resultados
            //Carrega o poster do filme
            let StrUrl: URL = URL(string: "http://image.tmdb.org/t/p/w500" + FilmesFiltrados[indexPath.row].posterPath)!

            celula.imagem.af.setImage(for: .normal, url: StrUrl)

            //Carrega o titulo do filme
            celula.tituloF?.text = FilmesFiltrados[indexPath.row].title
            //Carrega a releaseDate
            //Formatar data
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"

            let date: Date? = dateFormatterGet.date(from: FilmesFiltrados[indexPath.row].releaseDate)
            celula.releaseDate?.text = dateFormatter.string(from: date!)

            //Carrega a descricao
            celula.descricao?.text = FilmesFiltrados[indexPath.row].overview

        } else {
        
            //Carrega o poster do filme
            let caminhoPoster = "http://image.tmdb.org/t/p/w500" + favorito[indexPath.row].posterPath
            let StrUrl: URL = URL(string: caminhoPoster)!

            celula.imagem.af.setImage(for: .normal, url: StrUrl)

            //Carrega o titulo do filme
            celula.tituloF?.text = favorito[indexPath.row].title
            //Carrega a releaseDate
            //Formatar data
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"

            let date: Date? = dateFormatterGet.date(from: favorito[indexPath.row].releaseDate)
            celula.releaseDate?.text = dateFormatter.string(from: date!)
            //Carrega a descricao
            celula.descricao?.text = favorito[indexPath.row].overview
        }
        
        return celula

    }
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        //let messageLabel = UILabel(frame: CGRect(x: 50,y: 50,width: 300,height: 21))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
        messageLabel.sizeToFit()
        
        messageLabel.text = message
        messageLabel.textAlignment = .center
        
        let image: UIImage = UIImage(named: "smiley")!

        var bgImage: UIImageView?
        bgImage = UIImageView(image: image)
        bgImage!.frame = CGRect(x: 155,y: 220,width: 60,height: 60)
        
        messageLabel.addSubview(bgImage!)
        
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
