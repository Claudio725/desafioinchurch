//
//  MyCell.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 03/06/21.
//

import UIKit

class MyCell: UICollectionViewCell {
    @IBOutlet weak var imagem: UIButton!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var favorito: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
