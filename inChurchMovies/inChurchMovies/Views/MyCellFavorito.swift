//
//  MyCellFavorito.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 06/06/21.
//

import UIKit

class MyCellFavorito: UITableViewCell {
    @IBOutlet weak var imagem: UIButton!
    @IBOutlet weak var tituloF: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var descricao: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
