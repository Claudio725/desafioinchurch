//
//  Generos.swift
//  inChurchMovies
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 06/06/21.
//

import Foundation

import Foundation

struct Generos: Codable {
    let genres: [Genre]
}

struct Genre: Codable {
    let id: Int
    let name: String 
}
