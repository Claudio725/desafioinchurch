//
//  Movies.swift
//  inChurch
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 23/04/21.
//

import Foundation

struct Movies: Codable {
    let page: Int
    let results: [Result]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
        
}
