//
//  CodableBundleExtension.swift
//  inChurch challenge
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 23/04/21.
//

import Foundation

extension Bundle {
    func decode() -> Movies{
        //1 - Ler a API

        guard let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=448d0494c4ba7a27cbea095af577c796&language=en-US&page=1") else {
            fatalError("Falha em encontrar a api")
        }
        //2 - criar a propriedade para o data
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Falha ao carregar no bundle")
        }
        //3 - Criar o decoder
        let decoder = JSONDecoder()
        
        //4 - Criar a propriedade para o data decoded
        guard let loaded = try? decoder.decode(Movies.self, from: data) else {
            fatalError("Falha ao decodificar do bundle")
        }
        
        
        return  loaded
    }

    func decode_Generos() -> Generos{
        //1 - Ler a API

        guard let url = URL(string: "https://api.themoviedb.org/3/genre/movie/list?api_key=448d0494c4ba7a27cbea095af577c796&language=en-US&page=1") else {
            fatalError("Falha em encontrar a api")
        }
        //2 - criar a propriedade para o data
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Falha ao carregar no bundle")
        }
        //3 - Criar o decoder
        let decoder = JSONDecoder()
        
        //4 - Criar a propriedade para o data decoded
        guard let loaded = try? decoder.decode(Generos.self, from: data) else {
            fatalError("Falha ao decodificar do bundle")
        }
        
        
        return  loaded
    }
}
